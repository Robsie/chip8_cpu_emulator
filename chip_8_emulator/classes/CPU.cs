﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace chip_8_emulator
{
    class CPU
    {
        //Operations
        private delegate void operationDelegate();
        private Dictionary<string, operationDelegate> operations = new Dictionary<string, operationDelegate>();
        private GPU gpu = new GPU();
        
        //Memory
        byte[] memory = new byte[4096]; // 4k total memory, each 1 byte

    
        /* 
            Memory map
            0x000 - 0x1FF - chip 8 interpreter
            0x050 - 0x0A0 - build in 4x5 pixel font set (0-F)
            0x200 - 0xFFF - program ROM and work RAM
         */

        

        //ushort[] stack = new ushort[16];
        Stack<ushort> stack = new Stack<ushort>();

        //Registers
        byte[] v = new byte[16]; // registers v0 to vE are general purpouse, VF is the carry flag
        ushort index; //index register
        ushort program_counter; //program counter

        

        //When set, will count down to 0 at 60Hz
        ushort delay_timer;
        ushort sound_timer; //buzzer sounds whenever this reaches 0

        public string opcode; //current opcode, for calling the correct function
        public ushort instruction = Convert.ToUInt16("1010001011110000",2); //instruction is 2 bytes

        public DateTime clock;

        //private void initiateOperations()
        //{
        //    operations.Add("AND", new operationDelegate(AND));
        //    operations.Add("OR", new operationDelegate(OR));
        //    operations.Add("XOR", new operationDelegate(XOR));
        //    operations.Add("NOT", new operationDelegate(NOT));
        //    operations.Add("MOV", new operationDelegate(MOV));
        //    operations.Add("SET", new operationDelegate(SET));
        //    operations.Add("RANDOM", new operationDelegate(RANDOM));
        //    operations.Add("JMP", new operationDelegate(JMP));
        //    operations.Add("JZ", new operationDelegate(JZ));
        //}

        //INITIALISATION METHODS

        private void initialiseOperators()
        {
            operations.Add("00E0", new operationDelegate(OP_00E0));
            operations.Add("00EE", new operationDelegate(OP_00EE));
            operations.Add("1NNN", new operationDelegate(OP_1NNN));
            operations.Add("2NNN", new operationDelegate(OP_2NNN));
            operations.Add("3XNN", new operationDelegate(OP_3XNN));
            operations.Add("4XNN", new operationDelegate(OP_4XNN));
            operations.Add("5XY0", new operationDelegate(OP_5XY0));
            operations.Add("6XNN", new operationDelegate(OP_6XNN));
            operations.Add("7XNN", new operationDelegate(OP_7XNN));
            operations.Add("8XY0", new operationDelegate(OP_8XY0));
            operations.Add("8XY1", new operationDelegate(OP_8XY1));
            operations.Add("8XY2", new operationDelegate(OP_8XY2));
            operations.Add("8XY3", new operationDelegate(OP_8XY3));
            operations.Add("8XY4", new operationDelegate(OP_8XY4));
            operations.Add("8XY5", new operationDelegate(OP_8XY5));
            operations.Add("8XY6", new operationDelegate(OP_8XY6));
            operations.Add("8XY7", new operationDelegate(OP_8XY7));
            operations.Add("8XYE", new operationDelegate(OP_8XYE));
            operations.Add("9XY0", new operationDelegate(OP_9XY0));
            operations.Add("ANNN", new operationDelegate(OP_ANNN));
            operations.Add("BNNN", new operationDelegate(OP_BNNN));
            operations.Add("CXNN", new operationDelegate(OP_CXNN));
            operations.Add("DXYN", new operationDelegate(OP_DXYN));
            operations.Add("EX9E", new operationDelegate(OP_EX9E));
            operations.Add("EXA1", new operationDelegate(OP_EXA1));
            operations.Add("FX07", new operationDelegate(OP_FX07));
            operations.Add("FX0A", new operationDelegate(OP_FX0A));
            operations.Add("FX15", new operationDelegate(OP_FX15));
            operations.Add("FX18", new operationDelegate(OP_FX18));
            operations.Add("FX1E", new operationDelegate(OP_FX1E));
            operations.Add("FX29", new operationDelegate(OP_FX29));
            operations.Add("FX33", new operationDelegate(OP_FX33));
            operations.Add("FX55", new operationDelegate(OP_FX55));
            operations.Add("FX65", new operationDelegate(OP_FX65));
        }

        public CPU()
        {
            initialiseOperators();
        }

        public void run()
        {
            cycle();
            maintain_clock();
        }

        //RUN METHODS
        public void cycle()
        {
            //fetch opcode
            //decode opcode
            //execute opcode
            //update timers
        }

        private void fetch()
        {
            
            string temp = "";
           // ushort opcode = 0;
            temp += Convert.ToString(memory[program_counter], 2) + Convert.ToString( memory[program_counter+1], 2);
            if (temp.Count() == 16)
            {
                instruction = Convert.ToUInt16(temp);
            }
        }

        public void decode()
        {
            string prefix = Convert.ToString(instruction, 16).Substring(0,1).ToUpper();

            switch(prefix)
            {
                case "0":
                    if (Convert.ToString(instruction, 16).ToUpper() == "00E0") { opcode = "00E0"; }
                    else if (Convert.ToString(instruction, 16).ToUpper() == "00EE") { opcode = "00EE"; }
                    else { opcode = "0NNN"; }
                    break;
                case "1":
                    opcode = "1NNN";
                    break;
                case "2":
                    opcode = "2NNN";
                    break;
                case "3":
                    opcode = "1XNN";
                    break;
                case "4":
                    opcode = "4XNN";
                    break;
                case "5":
                    opcode = "5XY0";
                    break;
                case "6":
                    opcode = "6XNN";
                    break;
                case "7":
                    opcode = "7XNN";
                    break;
                case "8":
                    if (Convert.ToString(instruction, 16).Substring(3, 1).ToUpper() == "0") { opcode = "00E0"; }
                    else if (Convert.ToString(instruction, 16).Substring(3, 1).ToUpper() == "0") { opcode = "8XY0"; }
                    else if (Convert.ToString(instruction, 16).Substring(3, 1).ToUpper() == "1") { opcode = "8XY1"; }
                    else if (Convert.ToString(instruction, 16).Substring(3, 1).ToUpper() == "2") { opcode = "8XY2"; }
                    else if (Convert.ToString(instruction, 16).Substring(3, 1).ToUpper() == "3") { opcode = "8XY3"; }
                    else if (Convert.ToString(instruction, 16).Substring(3, 1).ToUpper() == "4") { opcode = "8XY4"; }
                    else if (Convert.ToString(instruction, 16).Substring(3, 1).ToUpper() == "5") { opcode = "8XY5"; }
                    else if (Convert.ToString(instruction, 16).Substring(3, 1).ToUpper() == "6") { opcode = "8XY6"; }
                    else if (Convert.ToString(instruction, 16).Substring(3, 1).ToUpper() == "7") { opcode = "8XY7"; }
                    else if (Convert.ToString(instruction, 16).Substring(3, 1).ToUpper() == "E") { opcode = "8XYE"; }
                    break;
                case "9":
                    opcode = "9XY0";
                    break;
                case "A":
                    opcode = "ANNN";
                    break;
                case "B":
                    opcode = "BNNN";
                    break;
                case "C":
                    opcode = "CXNN";
                    break;
                case "D":
                    opcode = "DXYN";
                    break;
                case "E":
                    if (Convert.ToString(instruction, 16).Substring(2, 1).ToUpper() == "9E") { opcode = "EX9E"; }
                    else if (Convert.ToString(instruction, 16).Substring(2, 1).ToUpper() == "A1") { opcode = "EXA1"; }
                    break;
                case "F":
                    if (Convert.ToString(instruction, 16).Substring(2, 1).ToUpper() == "9E") { opcode = "EX9E"; }
                    else if (Convert.ToString(instruction, 16).Substring(2, 1).ToUpper() == "07") { opcode = "FX07"; }
                    else if (Convert.ToString(instruction, 16).Substring(2, 1).ToUpper() == "0A") { opcode = "FX0A"; }
                    else if (Convert.ToString(instruction, 16).Substring(2, 1).ToUpper() == "15") { opcode = "FX15"; }
                    else if (Convert.ToString(instruction, 16).Substring(2, 1).ToUpper() == "18") { opcode = "FX18"; }
                    else if (Convert.ToString(instruction, 16).Substring(2, 1).ToUpper() == "1E") { opcode = "FX1E"; }
                    else if (Convert.ToString(instruction, 16).Substring(2, 1).ToUpper() == "29") { opcode = "FX29"; }
                    else if (Convert.ToString(instruction, 16).Substring(2, 1).ToUpper() == "33") { opcode = "FX33"; }
                    else if (Convert.ToString(instruction, 16).Substring(2, 1).ToUpper() == "55") { opcode = "FX55"; }
                    else if (Convert.ToString(instruction, 16).Substring(2, 1).ToUpper() == "65") { opcode = "FX65"; }
                    break;
                default:
                    opcode = "SHIT";
                    break;
                
            }
        }

        private void execute()
        {
            operations[opcode]();
        }

        private void update()
        {
        }

        public void maintain_clock()
        {
        }



        //OPERATOR METHODS



        //CLEAR SCREEN
        private void OP_00E0()
        {
            gpu.clear_screen();
        }
        //Return from subroutine
        private void OP_00EE()
        {
            program_counter = stack.Pop();
        }
        //Jump to address NNN
        private void OP_1NNN()
        {
            program_counter = Convert.ToUInt16(instruction & 0x0FFF);
        }
        //Call subroutine at NNN
        private void OP_2NNN()
        {
            stack.Push(program_counter);
            program_counter = Convert.ToUInt16(instruction & 0x0FFF);
        }
        //Skip the next instruction if VX == NN
        private void OP_3XNN()
        {
            if (v[instruction & 0x0F00] == (instruction & 0x00FF))
            {
                program_counter += 4;
            }
            else
            {
                program_counter += 2;
            }
            
        }
        //Skip the next instruction if VX does NOT = NN
        private void OP_4XNN()
        {
            if (v[instruction & 0x0F00] != (instruction & 0x00FF))
            {
                program_counter += 4;
            }
            else
            {
                program_counter += 2;
            }
        }
        //skip the next instruction if VX = VY
        private void OP_5XY0()
        {
            if (v[instruction & 0x0F00] == (v[instruction & 0x00F0]))
            {
                program_counter += 4;
            }
            else
            {
                program_counter += 2;
            }
        }
        //Set VX to NN
        private void OP_6XNN()
        {
            v[instruction & 0x0F00] = Convert.ToByte(instruction & 0x00FF);
        }
        //add NN to VX
        private void OP_7XNN()
        {
            v[instruction & 0x0F00] += Convert.ToByte(instruction & 0x00FF);
        }
        //sets VX to the value of VY
        private void OP_8XY0()
        {
            v[instruction & 0x0F00] = v[instruction & 0x00F0];
        }
        //sets VX to the value of VX OR VY
        private void OP_8XY1()
        {
            v[instruction & 0x0F00] |= v[instruction & 0x00F0];
        }
        //sets VX to the value of VX AND VY
        private void OP_8XY2()
        {
            v[instruction & 0x0F00] &= v[instruction & 0x00F0];
        }
        //sets VX to the value of VX XOR VY
        private void OP_8XY3()
        {
            v[instruction & 0x0F00] ^= v[instruction & 0x00F0];
        }
        //Adds VY to VX. VF is set to 1 when theres a carry, and to 0 when there is not
        private void OP_8XY4()
        {
            if (v[instruction & 0x0F00] + v[instruction & 0x00F0] > 255)
            {
                v[0xF] = 1;
                v[instruction & 0x0F00] = 255;
            }
            else
            {
                v[0xF] = 0;
                v[instruction & 0x0F00] += v[instruction & 0x00F0];
            }
           
        }
        //VY is subtracted from VX, VF is set to 0 when there is a borrow, 1 when there is not
        private void OP_8XY5()
        {
        }
        //Shifts VX to the right by one, VF is set to the value of the least significant bit of VX before the shift
        private void OP_8XY6()
        {
        }
        //Sets VX to VY minus VX. VF is set to - when there is a borrow, and 1 when there is not
        private void OP_8XY7()
        {
        }
        //shifts VX to the left by one. VF is set to the value of the most significant bit before the shift
        private void OP_8XYE()
        {
            v[0xF] = Convert.ToByte(Convert.ToString(v[instruction] & 0x0F00, 2).Substring(0,1));
            v[instruction & 0x0F00] = Convert.ToByte(v[instruction & 0x0F00] << 1);
        }

        //Skips the next instruction if VX does not equal VY
        private void OP_9XY0()
        {
            if (v[Convert.ToUInt16(instruction & 0x0F00)] != v[Convert.ToUInt16(instruction & 0x00F0)])
            {
                program_counter += 4;
            }
            else
            {
                program_counter += 2;
            }
            
        }

        //sets I to the address NNN
        private void OP_ANNN()
        {
            //index = Convert.ToByte(Convert.ToString(instruction).Substring(3, 12));
            index = Convert.ToUInt16(instruction & 0x0FFF);
        }
        //Jumps to the address NNN plus V0
        private void OP_BNNN()
        {
            program_counter = Convert.ToUInt16(Convert.ToUInt16(Convert.ToString(instruction).Substring(3, 12)) + v[0]);
        }
        //sets VX to a random number and NN
        private void OP_CXNN()
        {
            Random random = new Random();
            v[Convert.ToInt32(Convert.ToString(instruction,2).Substring(3,12))] = Convert.ToByte(random.Next(0, 255));
        }
        /* 
         * Draws a sprite at coordinate (VX, VY) that has a width of 8 pixels 
         * and a height of N pixels. Each row of 8 pixels is read as bit-coded 
         * (with the most significant bit of each byte displayed on the left) 
         * starting from memory location I; I value doesn't change after the 
         * execution of this instruction. As described above, VF is set to 1 if 
         * any screen pixels are flipped from set to unset when the sprite is 
         * drawn, and to 0 if that doesn't happen.
         */
        private void OP_DXYN()
        {

            v[0xF] = gpu.draw_sprite(instruction & 0x0F00, instruction & 0x00F0, instruction & 0x000F,8);
            
        }
        //Skips the next instruction if the key stored at VX is pressed
        private void OP_EX9E()
        {
            if (v[instruction & 0x0F00] == 1)
            {
                program_counter += 4;
            }
            else
            {
                program_counter += 2;
            }
        }
        //skips the next instruction if the key stored at VX is NOT pressed
        private void OP_EXA1()
        {
            if (v[instruction & 0x0F00] != 1)
            {
                program_counter += 4;
            }
            else
            {
                program_counter += 2;
            }
        }
        //sets VX to the value of the delay timer
        private void OP_FX07()
        {
            v[instruction & 0x0F00] = Convert.ToByte(delay_timer);
        }
        //awaits a keypress, and it is then stored in VX
        private void OP_FX0A()
        {
            v[instruction & 0x0F00] = Convert.ToByte(Console.ReadLine());
        }
        //Sets the delay timer to VX
        private void OP_FX15()
        {
            delay_timer = v[instruction & 0x0F00];
        }
        //sets the sound timer to VX
        private void OP_FX18()
        {
            sound_timer = v[instruction & 0x0F00];
        }

        //adds VX to I
        private void OP_FX1E()
        {
            index += v[instruction & 0x0F00];
        }

        /*
         * Sets I to the location of the sprite for the character in VX. 
         * Characters 0-F (in hexadecimal) are represented by a 4x5 font.
         */
        private void OP_FX29()
        {
        }

        /*
         *  	Stores the Binary-coded decimal representation of VX, 
         *  	with the most significant of three digits at the address in I, 
         *  	the middle digit at I plus 1, and the least significant digit 
         *  	at I plus 2. (In other words, take the decimal representation of 
         *  	VX, place the hundreds digit in memory at location in I, the tens 
         *  	digit at location I+1, and the ones digit at location I+2.)
         */
        private void OP_FX33()
        {
        }
        //stores V0 to VX in memory starting at address I
        private void OP_FX55()
        {
            for (int i = 0; i < (instruction & 0x0F00);i++ )
            {
                memory[i + index] = v[i];
            }
        }
        //Fills V0 to VX with values from memory starting at address I
        private void OP_FX65()
        {
            for (int i = 0; i < (instruction & 0x0F00); i++)
            {
                v[i] = memory[i + index];
            }
        }




    }
}
