﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace chip_8_emulator
{
    class GPU
    {
        const int pixel_count = 2048;
        byte[] pixels = new byte[pixel_count]; //2048 total pixels, can be 0 or 1

        public void clear_screen()
        {
            for (int i = 0; i < pixel_count; i++)
            {
                pixels[i] = 0;
            }
        }

        public byte draw_sprite(int x, int y,int height, int width) //returns a 1 if sprite overwrites existing byte
        {
            return 0;
        }
    }
}
