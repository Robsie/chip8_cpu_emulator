﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace chip_8_emulator
{
    class Program
    {
        static void Main(string[] args)
        {
            //byte[] v = new byte[16];
            //for (int i = 0; i < 16; i++)
            //{
            //    v[i] = Convert.ToByte(i);
            //}
            //v[0xE] = 74;
            //Console.Write(v[14]);
            //Console.ReadLine();

            byte part1 = 162;
            byte part2 = 240;
           // string temp = "";
            ushort opcode = 0;
           // temp += Convert.ToString(part1, 2) + Convert.ToString(part2, 2);
           // Console.WriteLine(temp);
           // opcode = Convert.ToUInt16(temp, 2);
            //opcode = Convert.ToUInt16((Convert.ToString(part1, 2) + Convert.ToString(part2, 2)));
            opcode = Convert.ToUInt16(part1 << 8);
            opcode |= part2;
            //Console.WriteLine(opcode);
            Console.WriteLine(Convert.ToString(opcode,16).ToUpper());
            CPU cpu = new CPU();
            cpu.decode();
            byte b1 = 0xFF;
            byte b2 = 0xFF;
            Console.WriteLine(Convert.ToByte(b1 + b2));
            //Console.WriteLine(cpu.opcode);

            Console.ReadLine();
        }
    }
}
